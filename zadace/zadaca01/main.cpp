#include <iostream>
#include <math.h> 
#include <algorithm>
class Pravokutnik;
class Tocka
{
	private:
		float x, y;
	public:
		Tocka(); // Inicijalizira tocku s kordinatama u ishodistu
		Tocka(int); // Inic. tocku kojoj su x i y koordinate jednake prosljedenoj vrijednosti
		Tocka(int, int); // Inicijalizira tocku s zadanim kooridnatama x i y.
		Tocka(const Tocka&); // Copy constructor
		void print(); // ispisuje koordinate tocke u obliku "(x,y)"
        float getX() const{return x;};
        float getY() const{return y;};
        void setX(float a){x=a;};
        void setY(float a){y=a;};
        float distance(const Tocka&) const;
        void zbroji(const Tocka&);
		bool isInside(const Pravokutnik&);
	// provjerava nalazi li se tocka unutar pravokutnika ili ne
};

Tocka::Tocka()
{
    x = 0;
    y = 0;
}

Tocka::Tocka(int a)
{
    x = a;
    y = a;
}

Tocka::Tocka(int a, int b)
{
    x = a;
    y = b;
}

Tocka::Tocka(const Tocka &T)
{
    x = T.x;
    y = T.y;
}

void Tocka::print()
{
    std::cout << "(" << x << "," << y << ")" << std::endl;
}

void Tocka::zbroji(const Tocka &T)
{
    x+=T.x;
    y+=T.y;
}

float Tocka::distance(const Tocka &T) const
{
    return sqrt(pow(x-T.x,2)+pow(y-T.y,2));
}


class Pravokutnik
{
	protected:
		Tocka A, B, C;
	public:
		Pravokutnik(const Pravokutnik&); // copy constructor
		Pravokutnik(const Tocka&, const Tocka&, const Tocka&); // inicijalizira pravokutnik sa zadanim tockama
		float povrsina() const;
		float opseg() const;
        Tocka nasuprot() const; 
		void translacija(const Tocka&); // translatira cetvrokut s obzirom na zadani radij-vektor
		void rotacija(float); //rotira prav. za zadani kut s obzirom na ishodiste koord. sustava
		void print();
        Tocka getA() const {return A;};
        Tocka getB() const {return B;};
        Tocka getC() const {return C;};
         // ispisuje tocke pravokutnik u obliku "A(x,y), B(x,y)"
};



Pravokutnik::Pravokutnik(const Pravokutnik &P)
{
    A = P.A;
    B = P.B;
    C = P.C;
}

Pravokutnik::Pravokutnik(const Tocka& T1, const Tocka& T2, const Tocka& T3)
{
    A = T1;
    B = T2;
    C = T3;
}

void Pravokutnik::translacija(const Tocka &T)
{
    A.zbroji(T);
    B.zbroji(T);
    C.zbroji(T);
}
void Pravokutnik::rotacija(float rot)
{
    Tocka T0;
    float r;
    r=A.distance(T0);
    A.setY(sin(asin(A.getY()/r)+rot)*r);
    A.setX(cos(acos(A.getX()/r)+rot)*r);
    r=B.distance(T0);
    B.setY(sin(asin(B.getY()/r)+rot)*r);
    B.setX(cos(acos(B.getX()/r)+rot)*r);
    r=C.distance(T0);
    C.setY(sin(asin(C.getY()/r)+rot)*r);
    C.setX(cos(acos(C.getX()/r)+rot)*r);
}

float Pravokutnik::povrsina() const
{
    return fabs(A.getX()*(B.getY()-C.getY())+B.getX()*(C.getY()-A.getY())+C.getX()*(A.getY()-B.getY()) );
}

Tocka Pravokutnik::nasuprot() const
{
    float distances[] = {A.distance(B),B.distance(C),C.distance(A)};
    if (*std::max_element(distances,distances+3)==distances[0])
    {
        Tocka T4(C);
        T4.setX(T4.getX()+abs(C.getX()-B.getX()));
        T4.setY(T4.getY()+abs(C.getX()-A.getX()));
        return T4;
    }
    else if (*std::max_element(distances,distances+3)==distances[1])
    {
        Tocka T4(A);
        T4.setX(T4.getX()+abs(A.getX()-B.getX()));
        T4.setY(T4.getY()+abs(A.getX()-C.getX()));
        return T4;
    }
    else if (*std::max_element(distances,distances+3)==distances[2])
    {
        Tocka T4(B);
        T4.setX(T4.getX()+abs(B.getX()-C.getX()));
        T4.setY(T4.getY()+abs(B.getX()-A.getX()));
        return T4;
    }
}

float Pravokutnik::opseg() const
{
    float distances[] = {A.distance(B),B.distance(C),C.distance(A)};
    if (*std::max_element(distances,distances+3)==distances[0])
    {
        return (C.distance(A)+C.distance(B))*2;
    }
    else if (*std::max_element(distances,distances+3)==distances[0])
    {
        return (A.distance(B)+A.distance(C))*2;
    }
    else if (*std::max_element(distances,distances+3)==distances[0])
    {
        return (B.distance(C)+B.distance(A))*2;
    }
}

void Pravokutnik::print()
{
    std::cout << "A(" << A.getX() << "," << A.getY() << ")," << " B(" << B.getX() << "," << B.getY() << ")," << " C(" << C.getX() << "," << C.getY() << ")" << std::endl;
}
bool Tocka::isInside(const Pravokutnik &P)
{
    Tocka T4(P.nasuprot());
    Tocka uljez(x,y);
    Pravokutnik P1(uljez,P.getA(),T4);
    Pravokutnik P2(uljez,P.getA(),P.getB());
    Pravokutnik P3(uljez,P.getC(),T4);
    Pravokutnik P4(uljez,P.getB(),P.getC());
    //std::cout<<P1.povrsina()/2<<"|"<<P2.povrsina()/2<<"|"<<P3.povrsina()/2<<"|"<<P4.povrsina()/2<<std::endl;
    if (P1.povrsina()/2+P2.povrsina()/2+P3.povrsina()/2+P4.povrsina()/2>P.povrsina())
    {
        return false;
    }else
    {
        return true;
    }


}
int main()
{ 
    int a;
    // Kvadrat stranice 1
    Tocka T1,T2(2),T3(2,0),T4(T3);
    Pravokutnik P1(T1,T2,T3),P2(P1);
    T1.print();
    T2.print();
    T3.print();
    T4.print();
    P1.print();
    P2.print();
    std::cout << P1.povrsina() << std::endl;
    std::cout << P1.opseg() << std::endl;
    P1.translacija(T2);
    P1.print();
    P1.rotacija(M_PI);
    P1.print();
    P1.rotacija(M_PI);
    P1.print();
    std::cout<<Tocka(0,0).isInside(P2);//true
    std::cout<<Tocka(-1,0).isInside(P2);//false
    std::cout<<Tocka(0.5,0).isInside(P2);//true
    std::cout<<Tocka(0.5,0.5).isInside(P2);//true
    std::cout<<Tocka(3.0,1.0).isInside(P2);//false
    std::cin >> a;
} 