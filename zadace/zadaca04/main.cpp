#include <iostream>
#include <math.h> 
#include <algorithm>
class Complex {
    protected:
        float re, im;
    public:
        Complex(float re = 0, float im = 0);

        float real() const { return re; };
        float imag() const { return im; };

        void real(float r) { re = r; };
        void imag(float i) { im = i; };

        Complex& operator=(const Complex&);
        Complex& operator+=(const Complex&);
        Complex& operator-=(const Complex&);
        Complex& operator*=(const Complex&);
        Complex& operator/=(const Complex&);

        Complex operator+(const Complex&) const;
        Complex operator-(const Complex&) const;
        Complex operator*(const Complex&) const;
        Complex operator/(const Complex&) const;

        bool operator==(const Complex&) const;
        bool operator!=(const Complex&) const;

        void print();
};

Complex::Complex(float re, float im)
{
    this->re=re;
    this->im=im;
}

void Complex::print()
{
    std::cout<< re << " " << im << "i" << std::endl;
}
Complex& Complex::operator=(const Complex& C)
{
    re = C.real();
    im = C.imag();

    return *this;
}

  Complex Complex::operator+(const Complex& C) const
{
      Complex result;
      result.re = (this->re + C.re);
      result.im = (this->im + C.im);
      return result;
}

  Complex Complex::operator-(const Complex& C) const
{
      Complex result;
      result.re = (this->re - C.re);
      result.im = (this->im - C.im);
      return result;
}

  Complex Complex::operator*(const Complex& C) const
{
      Complex result;
      result.re = (this->re * C.re - this->im * C.im);
      result.im = (this->im * C.re + this->re * C.im);
      return result;
}

  Complex Complex::operator/(const Complex& C) const
{
      Complex result;
      result.re = (this->re*C.re+this->im*C.im)/(C.re*C.re+C.im*C.im);
      result.im = (-this->re*C.im+this->im*C.re)/(C.re*C.re+C.im*C.im);
      return result;
}

bool Complex::operator==(const Complex& C) const
{
      return (this->re==C.re && this->im==C.im);
}

bool Complex::operator!=(const Complex& C) const
{
      return !(this->re==C.re && this->im==C.im);
}

Complex& Complex::operator+=(const Complex& C)
{
    this->re+=C.re;
    this->im+=C.im;
    return *this;
}

Complex& Complex::operator-=(const Complex& C)
{
    this->re-=C.re;
    this->im-=C.im;
    return *this;
}

Complex& Complex::operator*=(const Complex& C)
{
    float re = this->re;
    this->re=(this->re * C.re - this->im * C.im);
    this->im=(this->im * C.re + re * C.im);
    return *this;
}
Complex& Complex::operator/=(const Complex& C)
{
    float re = this->re;
    this->re= (this->re*C.re+this->im*C.im)/(C.re*C.re+C.im*C.im);
    this->im= (-re*C.im+this->im*C.re)/(C.re*C.re+C.im*C.im);
    return *this;
}

int main()
{ 
    int a;

    Complex C1,C2(2,2),C3(2,3),C4(5,4);
    C1=C2;
    C1.print();
    C2.print();
    C3.print();
    Complex R1;
    Complex R2;
    Complex R3;
    Complex R4;
    R1=C3+C4;
    R2=C3-C4;
    R3=C3*C4;
    R4=C3/C4;
    R1.print();
    R2.print();
    R3.print();
    R4.print();
    std::cout << (C1==C3) << " " << (C1==C2) << std::endl; //false true
    std::cout << (C1!=C3) << " " << (C1!=C2) << std::endl; //true false
    C1+=C2;
    C1.print();
    C1-=C3;
    C1.print();
    C1*=C4;
    C1.print();
    C1/=C4;
    C1.print();
    //std::cin >> a;
} 