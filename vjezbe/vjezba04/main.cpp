#include <iostream>
using namespace std;

class Stack
{
    public:
        int* container;
        int size;
        int top;

        Stack(int size);
        Stack(const Stack& s);
        ~Stack();

        bool isEmpty();
        void push(int x);
        int pop();


        void print();

};

Stack::Stack(int size) 
{
    this->size=size;
    container = new int[size];
}

Stack::~Stack()
{
    delete [] container;
}

Stack::Stack(const Stack& s)
{
    size = s.size;
    top = s.top;
    container = new int[size];
    for(int i = 0; i < size; ++i)
    {
        container[i] = s.container[i];

    }
}

bool Stack::isEmpty()
{
    return top == 0;

}

void Stack::print()
{
    for (int i=0; i<size; ++i)
    {
        cout << container[i] << "\t";
    }
    cout <<endl;
}

void Stack::push(int x)
{
    if(top < size)
    {
        container[top++] = x;
    }
}
int Stack::pop()
{
    return container[--top];
}
int main()
{
    int a;
    Stack s1(5);
    s1.print();
    cout<<(s1.isEmpty() ? "yep" : "nope")<<endl;
    s1.push(6);
    cout<<(s1.isEmpty() ? "yep" : "nope")<<endl;
    s1.print();

    s1.push(12);
    s1.push(6);

    s1.print();
    s1.push(5);
    s1.pop();
    s1.print();


    cin>>a;
    return 0;
}